<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

class States extends REST_Controller
{

    public function __construct($config = 'rest')
    {
        parent::__construct();
    }

    /**
     * select statelist
     */
    public function statelist_post()
    {
        $sqlSeletStateList = "SELECT `state` FROM statelist GROUP BY `state` ORDER BY `state`";
        $stateList = $this->api_model->custom_query($sqlSeletStateList);
        if ($stateList) {
            $statesSelectOption = '';
            foreach ($stateList as $state) {
                $statesSelectOption .= "<option value='" . $state['state'] . "'>" . $state['state'] . "</option>";
            }
            $message = array(
                "status" => 1,
                "message" => STATE_DATA,
                "data" => $statesSelectOption
            );
        } else {
            $message = array(
                "status" => 0,
                "message" => SOMETHING_WENT_ERONG
            );
        }
        $this->set_response($message, REST_Controller::HTTP_OK);
    }

    public function cityList_post(){
        $states = $this->input->post('state');
        $statesArray = explode(",", $states);
        $statesString = "'".implode("','", $statesArray)."'";
        $sqlSelectCities = "SELECT `city_name`, `state` FROM `statelist` WHERE `state` IN ($statesString)";
        $citiesList = $this->api_model->custom_query($sqlSelectCities);
        if ($citiesList) {
            $citiesSelectOption = '';
            foreach ($citiesList as $city) {
                $citiesSelectOption .= "<option value='" . $city['city_name'].",".$city['state']. "'>" . $city['city_name'] . "</option>";
            }
            $message = array(
                "status" => 1,
                "message" => CITY_DATA,
                "data" => $citiesSelectOption
            );
        } else {
            $message = array(
                "status" => 0,
                "message" => SOMETHING_WENT_ERONG
            );
        }
        $this->set_response($message, REST_Controller::HTTP_OK);

    }

}
