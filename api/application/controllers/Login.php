<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

class Login extends REST_Controller
{

    public function __construct($config = 'rest')
    {
        parent::__construct();
    }

    public function login_post()
    {
        if ($this->input->post('login_from') == "google") {
            $message = $this->_login_from_google();
        } else {
            $message = $this->_login_from_form();
        }
        $this->set_response($message, REST_Controller::HTTP_OK);
    }

    /**
     * login function from google
     * @return array consist of status and message as a key
     */
    function _login_from_google()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('userName', 'User name', 'required|trim');
        $this->form_validation->set_rules('api_key', 'Api Key', 'required|trim');
        if ($this->form_validation->run()) {

            $userId = $this->input->post('userName'); //getting post data userName == email
            $api_key = $this->input->post('api_key');//getting post data api_key
            $login_from = $this->input->post('login_from');//getting post data login_from

            //check email exist or not
            $checkEmail = $this->api_model->select('user', 'email', array('email =' => $userId), NULL, NULL, true, NULL);
            if (empty($checkEmail)) {
                $table_name = 'user'; //table name
                // post data
                $post_data = array(
                    "api_key" => $api_key,
                    "email" => $userId,
                    "login_from" => $login_from,
                );
                $is_udid = false; //setting udid to false
                $insertId = $this->api_model->insert($table_name, $post_data, $is_udid); // inserting user data
                if ($insertId) {
                    //Map session data to user
                    $message = $this->_map_session($userId);
                    if ($message['status'] == 1) {
                        $message = array(
                            "status" => 1,
                            "message" => USER_LOGGEDIN,
                        );
                    }
                } else {
                    $message = array(
                        "status" => 0,
                        "message" => SOMETHING_WENT_ERONG,
                    );
                }
            } else {
                $table_name = 'user'; // table name
                // data
                $data = array(
                    "api_key" => $api_key,
                );
                //condition
                $cond = array(
                    "email" => $userId
                );
                $UpdateApiKey = $this->api_model->update($table_name, $data, $cond); // update query
                if ($UpdateApiKey) {
                    //Map session data to user
                    $message = $this->_map_session($userId);
                    if ($message['status'] == 1) {
                        $message = array(
                            "status" => 1,
                            "message" => USER_LOGGEDIN,
                        );
                    }
                } else {
                    $message = array(
                        "status" => 0,
                        "message" => SOMETHING_WENT_ERONG,
                    );
                }
            }
        } else {
            $message = array(
                "status" => 0,
                "message" => SOMETHING_WENT_ERONG,
            );
        }
        return $message;
    }

    /**
     * private function
     * @return array  consist of status and message as keys
     */
    function _login_from_form()
    {
        $userId = $this->input->post('userName');
        $password = $this->input->post('password');
        if ($this->session->userdata('is_logged_in') == true) {
            $message = array(
                "status" => 1,
                "message" => USER_LOGGEDIN
            );
        } else {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('userName', 'User name', 'required|trim');
            $this->form_validation->set_rules('password', 'Pasword', 'required|trim');
            if ($this->form_validation->run()) {
                $selectUser = $this->api_model->select('user', 'session_id, password', array('email =' => $userId), NULL, NULL, true, NULL);
                if (empty($selectUser)) {
                    $message = array(
                        "status" => 0,
                        "message" => INVALID_COMBINATION,
                    );
                } else {
                    if (empty($selectUser['session_id'])) {
                        //varify password
                        if (password_verify($password, $selectUser['password'])) {
                            //Map session data to user
                            $message = $this->_map_session($userId);
                            if ($message['status'] == 1) {
                                $message = array(
                                    "status" => 1,
                                    "message" => USER_LOGGEDIN,
                                );
                            }
                        } else {
                            $message = array(
                                "status" => 0,
                                "message" => INVALID_COMBINATION,
                            );
                        }


                    } else {
                        $message = array(
                            "status" => 2,
                            "message" => ALREADY_LOGGEDIN,
                        );
                    }
                }
            } else {
                //form validation fails
                $message = array(
                    "status" => 0,
                    "message" => INVALID_COMBINATION
                );

            }
        }
        return $message;
    }

    /*
     * funcion to map new session data t ser and delete previous session
     */
    function proceed_post()
    {
        //username from the post data
        $userId = $this->input->post('userName');
        $message = $this->_map_session($userId);
        if ($message['status'] == 1) {
            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    /**
     * function to map user new sesson
     * @param $userId email id of user
     */
    function _map_session($userId)
    {
        //username from the post data
        $userId = $this->input->post('userName');
        $u_data = array(
            'userId' => $userId,
            'is_logged_in' => true
        );
        $this->session->set_userdata($u_data);
        $sessionId = session_id();
        //Map new sessionId with userId and destroy previous one
        $mapSessionResturn = $this->api_model->setSession($userId, $sessionId);
        if ($mapSessionResturn) {
            $message = array(
                "status" => 1,
                "message" => USER_LOGGEDIN,
            );
        }else{
            $message = array(
                "status" => 0,
                "message" => USER_LOGGEDIN,
            );
        }

        return $message;
    }

    /**
     * check user already logged in or not
     */
    function check_login_post()
    {
        if ($this->session->userdata('is_logged_in') == true) {
            $message = array(
                "status" => 1
            );
        } else {
            $message = array(
                "status" => 0
            );
        }
        $this->set_response($message, REST_Controller::HTTP_OK);
    }
}
