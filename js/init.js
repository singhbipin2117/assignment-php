$(document).ready(function () {
    window.phpUrl = "http://ajmaurya.com/bipin/assignment-php/api/";
    var alreadyLoggedInMessage;
    var infowindow;
    var marker, map;

    /**
     * common function to call ajax request
     *
     * @param method GET or POST
     * @param url Url of the api
     * @param data data  to be pass
     * @returns {*}
     */
    window.ajaxCall = function (method, url, data) {
        return $.ajax({
            type: method,
            url: url,
            data: data
        });
    }
    /**
     * initializa map function
     */
    window.initMap = function () {
        var myCenter = new google.maps.LatLng(21.7679, 78.8718);
        var mapProp = {
            center: myCenter,
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map"), mapProp);

    };

    window.marker = function (locations) {
        var infowindow = new google.maps.InfoWindow();
        var marker, i;
        var locationLength = locations.length;
        var setMarkerOnMap = function (i = 0, locationlength) {
            var mapUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=india," + locations[i][0];
            method = "GET";
            ajaxCall(method, mapUrl, null)
                .done(function (data) {
                    if (data['status'] == 'OK') {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(data['results'][0]['geometry']['location']['lat'], data['results'][0]['geometry']['location']['lng']),
                            map: map
                        });

                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                var content = "<div><p><b>State : </b>" + locations[i][1] + "</p><p><b>City : </b>" + locations[i][0] + "</p><p><b>Latitude : </b>" + data['results'][0]['geometry']['location']['lat'] + "</p><p><b>Longitude : </b>" + data['results'][0]['geometry']['location']['laang'] + "</p></div>"
                                infowindow.setContent(content);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        i++;
                        if (i < locationLength) {
                            setMarkerOnMap(i, locationLength);
                        }
                    }
                });
        };
        setMarkerOnMap(0, locationLength);
    };
    /**
     * condition checking for page
     */
    if (window.location.pathname.split('/').indexOf('map_locate.php') != -1) {

        /**
         * check login function
         */
        window.checkLoggedIn = function () {
            var method = "POST";
            var url = phpUrl + "login/check_login";
            var data = null;
            ajaxCall(method, url, data)
                .done(function (response) {
                    if (response.status == 0) {
                        window.location.href = window.location.origin + "/bipin/assignment-php";
                    }

                });
        };
        //check user loggedin or not
        checkLoggedIn();


        //map initializaton function
        initMap();

        //select picker
        var stateList = ajaxCall('POST', phpUrl + "states/statelist", null)
        stateList.done(function (response) {
            if (response.status == 1) {
                $("#states").html(response.data);
            }
        });

        /**
         * event fired after state selection
         */
        $('#states').on('changed.bs.select', function (e) {
            var selectedState = $(this).val();
            var data = "state=" + selectedState;
            var stateList = ajaxCall('POST', phpUrl + "states/cityList", data);
            stateList.done(function (response) {
                if (response.status == 1) {
                    $("#city").html(response.data).selectpicker('refresh');
                }
            });
        });
        var cityList = [];
        /**
         * event fired after city selection
         */
        $('#city').on('changed.bs.select', function (e) {
            var selectedCity = $(this).val();
            var location = [];
            cityList = selectedCity.map(function (currentValue, index, arr) {
                return currentValue.split(",");
            });

        });
        /**
         * event fired after submit button clicked
         */
        $("#submit").click(function () {
            window.marker(cityList);
        });
    } else {
        /**
         * Login form submission start
         */
        $("#loginForm").validator().on("submit", function (event) {
            if (event.isDefaultPrevented()) {
                // handle the invalid form...
                formError();
                submitMSG(false, "Did you fill in the form properly?");
            } else {
                // everything looks good!
                event.preventDefault();
                submitForm();
            }
        });

        function submitForm() {
            // Initiate Variables With Form Content
            var userName = $("#userName").val();
            var password = $("#password").val();
            var method = "POST";
            var url = phpUrl + "login/login";
            var login_from = "form";
            var data = "userName=" + userName + "&password=" + password + "&login_from=" + login_from;
            //Ajax call function for login form submission
            ajaxCall(method, url, data).done(function (text) {
                if (text.status == "1") {
                    formSuccess();
                    submitMSG(true, text.message);
                    setTimeout(function () {
                        window.location.href = window.location.href + "map_locate.php";
                    }, 1000);
                } else if (text.status == 2) {
                    alreadyLoggedInMessage = text.message;
                    //trigger modal show
                    $('#myModal').modal('show');
                } else {
                    formError();
                    submitMSG(false, text.message);
                }
            }).fail(function () {
                formError();
                submitMSG(false, 'Oops! something went wrong.');
            });
        }

        /**
         * trigger when bootstrap modal open
         */
        $('#myModal').on('show.bs.modal', function (event) {
            var modal = $(this);
            modal.find('.modal-body').text(alreadyLoggedInMessage);
        });

        $('#proceed').click(function () {
            var userName = $("#userName").val();
            //Ajax call function for new devicelogin for same user.
            var method = "POST";
            var url = phpUrl + "login/proceed";
            var data = "userName=" + userName;
            ajaxCall(method, url, data).done(function (text) {
                console.log(text);
                if (text.status == "1") {
                    window.location.href = window.location.href + "/map_locate.php";
                }
            }).fail(function () {
                formError();
                submitMSG(false, 'Oops! something went wrong.');
            });
        });

        /**
         * formSuccess Method
         */
        function formSuccess() {
            $("#loginForm")[0].reset();
            submitMSG(true, "Message Submitted!")
        }

        /**
         * formerror method
         */
        function formError() {
            $("#loginForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                $(this).removeClass();
            });
        }

        /**
         * submitMSG method
         *
         * @param valid true or false
         * @param msg text message
         */
        function submitMSG(valid, msg) {
            if (valid) {
                var msgClasses = "h3 text-center tada animated text-success";
            } else {
                var msgClasses = "h3 text-center text-danger";
            }
            $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
        }

        /**
         *
         * Login form submision end
         */
    }


});

if (window.location.pathname.split('/').indexOf('map_locate.php') == -1) {
    /**
     * Googlelogin button
     */

    /**
     *
     * OnSuccess function tomgenerate google login button
     *
     * @param googleUser
     */
    function onSuccess(googleUser) {
        var profile = googleUser.getBasicProfile();
        gapi.client.load('plus', 'v1', function () {
            var request = gapi.client.plus.people.get({
                'userId': 'me'
            });
            //Display the user details
            request.execute(function (resp) {
                console.log(resp);
                var method = "POST";
                var url = phpUrl + "login/login";
                var login_from = "google";
                var data = "userName=" + resp.emails[0].value + "&api_key=" + resp.id + "&login_from=" + login_from;
                ajaxCall(method, url, data)
                    .done(function (response) {
                        if (response.status == 1) {
                            window.location.href = window.location.href + "map_locate.php"
                        }
                    });

            });
        });
    }

    /**
     * function called if there is erroron creating google signin button
     *
     * @param error
     */
    function onFailure(error) {
        alert(error);
    }

    /**
     * renderButton to generate google sign in button
     */
    function renderButton() {
        gapi.signin2.render('gSignIn', {
            'scope': 'profile email',
            'width': 240,
            'height': 50,
            'longtitle': true,
            'theme': 'dark',
            'onsuccess': onSuccess,
            'onfailure': onFailure
        });
    }

    /**
     * signOut button
     *
     */
    function signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            $('.userContent').html('');
            $('#gSignIn').slideDown('slow');
        });
    }
}