<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login | Assignment-php</title>
    <meta name="google-signin-client_id"
          content="73563656649-2dfuqf9n443slcupq2lth531pot7b8e7.apps.googleusercontent.com">
    <!-- Latest compiled and minified bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>

</head>
<body>

<div class="container ">
    <div class="row">
        <div class="col-md-12 text-center">
            <h2>MAP</h2>
            <div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-2">
                    <div class="col-md-3 text-center">
                        <select class="selectpicker" id="states" multiple>
                        </select>

                    </div>
                    <div class="col-md-3 text-center">
                        <select class="citypicker selectpicker" id="city" multiple>
                        </select>
                    </div>
                    <div class="col-md-3 text-center">
                        <button class="btn btn-block btn-primary" id="submit">Submit</button>
                    </div>
                </div>
            </div>

            <div class="row map-info">
                <div class="col-md-12">
                    <div id="map"></div>
                </div>
            </div>
        </div>


        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXfwUkCP9dL7-3h1b2SojARWtmRTMex-M"
                async defer></script>
        <!-- Latest jquery minified css -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"
                integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <!-- Latest compiled and minified bootstrap JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
                crossorigin="anonymous"></script>
        <script src="https://apis.google.com/js/client:platform.js?onload=renderButton" async defer></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
        <!-- Init.js -->
        <script src="js/init.js"></script>
</body>
</html>